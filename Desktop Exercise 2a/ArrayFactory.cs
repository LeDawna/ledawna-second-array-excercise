﻿using System;
using System.Linq;

namespace Desktop_Exercise_2a
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of random decimals
    /// </summary>
    /// <param name="size"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static decimal[] GetArray(int size, decimal min, decimal max)
    {
      var arr = new decimal[size];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween(min, max);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    /// <param name="arr"></param>
    public static void OutputArray(decimal[] arr)
    {
      Array.ForEach(arr, a => Console.WriteLine(string.Format("{0:C}", a)));
    }

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal AverageArrayValue(decimal[] arr)
    {
      return arr.Average();
    }

    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal MaxArrayValue(decimal[] arr)
    {
      var maxVal = (decimal)0;

      foreach (var a in arr)
      {
        if (a > maxVal)
        {
          maxVal = a;
        }
      }

      return maxVal;
    }
    //my code//

    //return arr.Max();

    //my code//

    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal MinArrayValue(decimal[] arr)
    {
      var minVal = arr[0];
      var i = 0;

      while (i < arr.Length)
      {
        if (arr[i] < minVal)
        {
          minVal = arr[i];
        }

        i += 1;
      }

      return minVal;
    }

    //my code start//

    //return arr.Min();

    //my code end//

    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    /// <param name="arr"></param>
    public static void SortArrayAsc(decimal[] arr)
    {
      //loop through the entire array
      for (var i = 0; i < arr.Length; i++)
      {
        //loop through the array's "next" elements
        for (var j = 0; j < arr.Length - 1; j++)
        {
          //if the item at j is greater than the next item, switch the items around
          if (arr[j] > arr[j + 1])
          {
            var x = arr[j + 1];

            arr[j + 1] = arr[j];
            arr[j] = x;
          }
        }
      }
    }

    // my Code//

    //{
    // Array.Sort(arr);
    // }
    //my code//

    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));

    }

    //sort decending//

    public static decimal[] SortArrayDesc(decimal[] arr)
    {
      return arr.OrderByDescending(x => x).ToArray<decimal>();
    }


  }
}
//}



