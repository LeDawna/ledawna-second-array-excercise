﻿using System;

namespace Desktop_Exercise_2a
{
  class Program
  {
    static void Main(string[] args)
    {
      ArrayTester();

      Console.Read();
    }

    private static decimal[] ArrayGenerator()
    {
      Console.WriteLine("enter array size");

      var size = Convert.ToInt32(Console.ReadLine());

      Console.WriteLine("enter array min value");

      var min = Convert.ToDecimal(Console.ReadLine());

      Console.WriteLine("enter array max value");

      var max = Convert.ToDecimal(Console.ReadLine());

      return ArrayFactory.GetArray(size, min, max);
    }

    private static void ArrayTester()
    {
      var arr = ArrayGenerator();

      Console.WriteLine("**** OutputArray ****");

      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n**** AverageArrayValue ****");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.AverageArrayValue(arr)));

      Console.WriteLine("\r\n**** MinArrayValue ****");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.MinArrayValue(arr)));

      Console.WriteLine("\r\n**** MaxArrayValue ****");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.MaxArrayValue(arr)));

      Console.WriteLine("\r\n**** SortArrayAsc ****");

      ArrayFactory.SortArrayAsc(arr);

      ArrayFactory.OutputArray(arr);
    }
  }
}
